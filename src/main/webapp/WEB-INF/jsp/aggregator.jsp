<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Cars Agregator</title>
</head>
<body>
    <c:forEach items="${data}" var="entry">
    <p> ${entry.key.name}</p>
            <ul>
                <c:forEach items="${entry.value}" var="model">
                    <li>
                       ${model.name}
                    </li>
                </c:forEach>
            </ul>
    </c:forEach>
</body>
</html>