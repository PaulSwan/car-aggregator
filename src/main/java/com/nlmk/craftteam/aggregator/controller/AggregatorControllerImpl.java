package com.nlmk.craftteam.aggregator.controller;

import com.nlmk.craftteam.aggregator.service.AggregateService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;


@RestController
@RequestMapping("/")
@Transactional
public class AggregatorControllerImpl implements AggregatorController {

    private final AggregateService aggregateService;

    public AggregatorControllerImpl(AggregateService aggregateService) {
        this.aggregateService = aggregateService;
    }

    @Override
    @GetMapping("/aggregator")
    public ModelAndView getAllBrandsAndModels() {
        ModelAndView model = new ModelAndView();
        model.setViewName("aggregator");
        model.addObject("data", aggregateService.getAllAggregateData());
        return model;
    }
}