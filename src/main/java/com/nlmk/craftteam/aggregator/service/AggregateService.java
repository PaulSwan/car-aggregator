package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.service.dto.Aggregator;
import com.nlmk.craftteam.aggregator.service.dto.Brand;
import com.nlmk.craftteam.aggregator.service.dto.Model;

import java.util.List;
import java.util.Map;

public interface AggregateService {
    Aggregator save(String hostName, Brand brand, List<Model> models);

    Map<Brand, List<Model>> getAllAggregateData();
}
