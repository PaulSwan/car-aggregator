package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.service.dto.Aggregator;
import com.nlmk.craftteam.aggregator.service.dto.Brand;
import com.nlmk.craftteam.aggregator.service.dto.Model;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class UpdateDbService {
    @Value("#{'${car.hosts}'.split(',')}")
    private List<String> hosts;
    private final AggregateService aggregateService;

    public UpdateDbService(AggregateService aggregateService) {
        this.aggregateService = aggregateService;
    }

    @Scheduled(fixedRate = 600000)
    public void launchUpdateDb() {
        log.info("Start update. Current time is :: " + LocalDateTime.now());
        for (String host : hosts) {
            handleHost(host);
        }
        log.info("Finished update. Current time is :: " + LocalDateTime.now());
    }

    private void handleHost(String host) {
        try {
            String brandsUrl = host + "/brands";
            ResponseEntity<Brand[]> brandsResponse = recieveExternalBrandsByHost(brandsUrl);
            handleExternalBrands(host, brandsResponse, brandsUrl);
        } catch (Exception e) {
            log.error("{}/brands is not available :: ", host, e);
        }
    }

    private ResponseEntity<Brand[]> recieveExternalBrandsByHost(String brandsUrl) {
        log.info("Request {} started...", brandsUrl);
        ResponseEntity<Brand[]> brandsResponse = new RestTemplate().getForEntity(brandsUrl, Brand[].class);
        log.info("Request {} finished with response: {}", brandsUrl, brandsResponse);
        return brandsResponse;
    }

    private void handleExternalBrands(String host, ResponseEntity<Brand[]> brandsResponse, String brandsUrl) {
        Brand[] brandsExt = brandsResponse.getBody();
        if (brandsResponse.getStatusCode() == HttpStatus.OK && brandsExt != null) {
            for (Brand brandExt : brandsExt) {
                hanleExternalBrand(host, brandExt);
            }
        } else {
            log.error("Request {} returned the next error code: {}", brandsUrl, brandsResponse.getStatusCode());
        }
    }

    private void hanleExternalBrand(String host, Brand brandExt) {
        try {
            String modelUrl = host + "/models/{brandId}";
            ResponseEntity<Model[]> modelsResponse = receiveExternalModels(brandExt, modelUrl);
            saveAggregateData(host, brandExt, modelUrl, modelsResponse);
        } catch (Exception e) {
            log.error("{}/models/{} is not available", host, brandExt.getId());
        }
    }

    private ResponseEntity<Model[]> receiveExternalModels(Brand brandExt, String modelUrl) {
        log.info("Request {} started...", modelUrl);
        ResponseEntity<Model[]> modelsResponse = new RestTemplate().getForEntity(modelUrl, Model[].class, brandExt.getId());
        log.info("Request {} finished with response: {}", modelUrl, modelsResponse);
        return modelsResponse;
    }

    private void saveAggregateData(String host, Brand brandExt, String modelUrl, ResponseEntity<Model[]> modelsResponse) {
        Model[] models = modelsResponse.getBody();
        if (modelsResponse.getStatusCode() == HttpStatus.OK && models != null) {
            List<Model> modelsInt = Arrays.stream(models).map(m ->
                    Model.builder().name(m.getName()).build()
            ).collect(Collectors.toList());

            Aggregator saved = aggregateService.save(host, brandExt, modelsInt);
            log.info("New data saved :: {}", saved);
        } else {
            log.error("Request {} returned the next error code: {}", modelUrl, modelsResponse.getStatusCode());
        }
    }
}