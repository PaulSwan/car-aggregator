package com.nlmk.craftteam.aggregator.service.impl;

import com.nlmk.craftteam.aggregator.repository.CarBrandRepository;
import com.nlmk.craftteam.aggregator.repository.CarModelRepository;
import com.nlmk.craftteam.aggregator.repository.HostRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import com.nlmk.craftteam.aggregator.repository.entity.Host;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import com.nlmk.craftteam.aggregator.service.AggregateService;
import com.nlmk.craftteam.aggregator.service.dto.Aggregator;
import com.nlmk.craftteam.aggregator.service.dto.Brand;
import com.nlmk.craftteam.aggregator.service.dto.Model;
import com.nlmk.craftteam.aggregator.util.AggregatorConverter;
import com.nlmk.craftteam.aggregator.util.CarFinderUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
@Slf4j
public class AggregateServiceImpl implements AggregateService {

    private final HostRepository hostRepository;
    private final CarModelRepository carModelRepository;
    private final CarBrandRepository carBrandRepository;
    private final AggregatorConverter aggregatorConverter;

    @Autowired
    public AggregateServiceImpl(
            HostRepository hostRepository,
            CarModelRepository carModelRepository,
            CarBrandRepository carBrandRepository,
            AggregatorConverter aggregatorConverter) {
        this.hostRepository = hostRepository;
        this.carModelRepository = carModelRepository;
        this.carBrandRepository = carBrandRepository;
        this.aggregatorConverter = aggregatorConverter;
    }

    @Override
    public Aggregator save(String hostName, Brand brand, List<Model> models) {
        List<CarModel> newModels = models.stream()
                .filter(mExt -> isNewModel(mExt))
                .map(mNew -> saveNewModel(brand, mNew))
                .collect(Collectors.toList());
        Host hostDbForUpdate = getHostDb(hostName, newModels);

        log.info("New car model {} save operation started...", hostDbForUpdate);
        hostDbForUpdate.getCarModels().addAll(newModels);
        hostDbForUpdate.setLastUpdateTime(LocalDateTime.now());
        Host saved = hostRepository.save(hostDbForUpdate);
        log.info("New car model save operation finished. Result :: {}", hostDbForUpdate);
        return aggregatorConverter.convert(saved);
    }

    @Override
    public Map<Brand, List<Model>> getAllAggregateData() {
        List<CarBrand> brandEntities = carBrandRepository.findAll();
        List<CarModel> modelEntities = carModelRepository.findAll();
        return brandEntities.stream().map(be ->
                Brand.builder()
                        .id(be.getId())
                        .name(be.getName())
                        .models(modelEntities.stream()
                                .filter(me -> Objects.equals(me.getCarBrand().getName(), be.getName()))
                                .map(me ->
                                        Model.builder()
                                                .id(me.getId())
                                                .name(me.getName())
                                                .build())
                                .collect(Collectors.toList()))
                        .build())
                .collect(HashMap::new, (map, brand) -> map.put(brand, brand.getModels()), HashMap::putAll);
    }

    private CarModel saveNewModel(Brand brand, Model modelDto) {
        CarBrand brandEntity = carBrandRepository.findOne(Example.of(
                CarBrand.builder().name(brand.getName()).build(),
                CarFinderUtil.getNameIgnoreCaseFindMatcher()))
                .or(() -> saveNewBrand(brand)).orElseThrow();

        CarModel newModel = CarModel.builder()
                .id(0L)
                .name(modelDto.getName())
                .carBrand(brandEntity)
                .hosts(new ArrayList<>())
                .build();
        log.info("New car model {} save operation started...", newModel);
        CarModel saved = carModelRepository.save(newModel);
        log.info("New car model save operation finished. Result :: {}", saved);
        return saved;
    }

    private Optional<CarBrand> saveNewBrand(Brand brand) {
        CarBrand newBrand = CarBrand.builder().id(0L).name(brand.getName()).build();
        log.info("New car brand {} save operation started...", newBrand);
        CarBrand saved = carBrandRepository.save(newBrand);
        log.info("New car brand save operation finished. Result :: {}", saved);
        return Optional.of(saved);
    }

    private boolean isNewModel(Model mExt) {
        return carModelRepository.findOne(Example.of(
                CarModel.builder().name(mExt.getName()).build(),
                CarFinderUtil.getNameIgnoreCaseFindMatcher())).isEmpty();
    }

    private Host getHostDb(String hostName, List<CarModel> newModels) {
        return hostRepository.findOne(Example.of(
                Host.builder().name(hostName).build(),
                CarFinderUtil.getNameIgnoreCaseFindMatcher()))
                .or(() -> saveNewHost(hostName, newModels))
                .orElseThrow(() -> {
                    log.error("Can't save host {}", hostName);
                    return new RuntimeException(String.format("Can't save host %s", hostName));
                });
    }

    private Optional<Host> saveNewHost(String hostName, List<CarModel> newModels) {
        log.info("New host {} save operation started...", hostName);
        Host newHost = Host.builder()
                .id(0L)
                .name(hostName)
                .carModels(newModels)
                .lastUpdateTime(LocalDateTime.now()).build();
        Optional<Host> saved = Optional.of(hostRepository.save(newHost));
        if (!saved.isPresent()) {
            log.error("Can't save host {}", hostName);
            throw new RuntimeException(String.format("Can't save host %s", hostName));
        }
        log.info("New host {} save operation finished. Result :: {}", saved.get());
        return saved;
    }
}
