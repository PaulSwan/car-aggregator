package com.nlmk.craftteam.aggregator.service.impl;

import com.nlmk.craftteam.aggregator.service.CarModelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarModelServiceImpl implements CarModelService {
}
