package com.nlmk.craftteam.aggregator.repository;

import com.nlmk.craftteam.aggregator.repository.entity.Host;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HostRepository extends JpaRepository<Host, Long> {

    List<Host> findByName(String hostName);
}
