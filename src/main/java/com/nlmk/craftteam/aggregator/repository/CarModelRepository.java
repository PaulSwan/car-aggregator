package com.nlmk.craftteam.aggregator.repository;

import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {

    List<CarModel> findByHostsName(String hostName);
}
