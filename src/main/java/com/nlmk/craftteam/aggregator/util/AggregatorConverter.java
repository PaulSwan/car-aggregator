package com.nlmk.craftteam.aggregator.util;

import com.nlmk.craftteam.aggregator.repository.entity.Host;
import com.nlmk.craftteam.aggregator.service.dto.Aggregator;

public interface AggregatorConverter {
    Aggregator convert(Host host);
}
