package com.nlmk.craftteam.aggregator.util;

import org.springframework.data.domain.ExampleMatcher;

public final class CarFinderUtil {
    private CarFinderUtil() {
    }

    public static ExampleMatcher getNameIgnoreCaseFindMatcher() {
        return ExampleMatcher.matching()
                .withIgnorePaths("id")
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.ignoreCase());
    }
}
