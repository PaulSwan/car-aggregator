package com.nlmk.craftteam.aggregator.util;

import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import com.nlmk.craftteam.aggregator.repository.entity.Host;
import com.nlmk.craftteam.aggregator.service.dto.Aggregator;
import com.nlmk.craftteam.aggregator.service.dto.Brand;
import com.nlmk.craftteam.aggregator.service.dto.Model;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AggregatorConverterImpl implements AggregatorConverter {

    @Override
    public Aggregator convert(Host host) {
        return Aggregator.builder()
                .host(host.getName())
                .brands(buildAllBrandsByHost(host))
                .build();
    }

    private List<Brand> buildAllBrandsByHost(Host host) {
        List<CarModel> carModels = host.getCarModels();
        return carModels.stream()
                .collect(Collectors.groupingBy(m -> m.getCarBrand(), Collectors.counting()))
                .entrySet().stream()
                .map(e -> buildBrand(carModels, e.getKey()))
                .collect(Collectors.toList());
    }

    private Brand buildBrand(List<CarModel> carModels, CarBrand brandEntity) {
        return Brand.builder()
                .id(brandEntity.getId())
                .name(brandEntity.getName())
                .models(getModelsByBrandName(carModels, brandEntity.getName()))
                .build();
    }

    private List<Model> getModelsByBrandName(List<CarModel> carModels, String currentBrandName) {
        return carModels.stream()
                .filter(m -> Objects.equals(m.getCarBrand().getName(), currentBrandName))
                .map(m -> Model.builder().id(m.getId()).name(m.getName()).build())
                .collect(Collectors.toList());
    }
}
